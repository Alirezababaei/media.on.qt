#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}
class QMediaPlayer;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_ar_clicked();

    void on_pyer_clicked();

    void on_pleis_clicked();

    void on_stop_clicked();

    void on_mut_clicked();

    void on_volom_valueChanged(int value);

private:
    Ui::MainWindow *ui;
    QMediaPlayer *mMediaPlayer;
};

#endif // MAINWINDOW_H
