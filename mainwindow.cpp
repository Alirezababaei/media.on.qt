###برای رفع مشکل برنامه کمک کنید

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMediaPlayer>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mMediaPlayer =new QMediaPlayer(this);
    connect(mMediaPlayer, &QMediaPlayer::positionChanged, [&](qint64 pos ){
        ui->avans->setValue(pos);
    });
    connect(mMediaPlayer,&QMediaPlayer::positionChanged,[&](qint64 dur){
        ui->avans->setMaximum(dur);
    });

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_ar_clicked()
{
   QString filenume = QFileDialog::getOpenFileName(this,"open file");
if(filenume.isEmpty()){
    return;
}
mMediaPlayer->setMedia(QUrl::fromLocalFile(filenume));
mMediaPlayer->setVolume(ui->volom->value());
on_pyer_clicked();
}

void MainWindow::on_pyer_clicked()
{
 mMediaPlayer->play();

}

void MainWindow::on_pleis_clicked()
{
mMediaPlayer->pause();
}

void MainWindow::on_stop_clicked()
{
mMediaPlayer->stop();
}

void MainWindow::on_mut_clicked()
{
    if(ui->mut->text()=="mute"){
    mMediaPlayer->setMuted(true);
   ui->mut->setText("unmute");
    }else{
    mMediaPlayer->setMuted(false);
ui->mut->setText("mute");
    }
}

void MainWindow::on_volom_valueChanged(int value)
{
mMediaPlayer->setVolume(value);
}
